Meteor Discovery
================

Web application created following on the Meteor Discovery book.

Deployed on http://canales-discover.meteor.com/

### Resources:
- [**Meteor** - Documentation](https://docs.meteor.com/#/basic/)
- [**Meteor** - Guide to templates Data Contexts](https://www.discovermeteor.com/blog/a-guide-to-meteor-templates-data-contexts/)
- [**Bootstrap** Themes](http://bootswatch.com/)

### Useful tricks/tips

#### Insecure
To enable non-logged users to create posts
```sh
$ meteor add insecure
```
else
```sh
$ meteor remove insecure
```

#### Autorun
```javascript
// Checks for changes in Session.get('pageTitle') 
Tracker.autorun( 
  function() { 
    console.log('Value is: ' + Session.get('pageTitle')); 
  } 
);
```

#### Events JQuery/Meteor
```javascript
//How to call events to 'header' template

//Meteor way 
//Docs at https://docs.meteor.com/#/full/eventmaps
Template.header.events({
  'click #btnSubmit': function(){
    alert("Button Clicked");
  } 
});

//JQuery way
//'rendered' waits for all elements to appear
Template.header.rendered = function() {
  $( "#btnSubmit" ).on( "click", function() {
   alert( "Button Clicked Too" );
  });
}
```

#### **Form Submission** _with serverside method_

```html
<!-- form view, in client/templates/posts/post_submit.html -->
<template name="postSubmit">
  <div class="col-md-12">
    <form class="main form">
      <div class="form-group">
        <label class="control-label" for="url">URL</label>
        <div class="controls">
          <input name="url" id="url" type="text" value="" placeholder="Your URL" class="form-control"/>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label" for="title">Title</label>
        <div class="controls">
          <input name="title" id="title" type="text" value="" placeholder="Name your post" class="form-control"/>
        </div>
      </div>
      <input type="submit" value="Submit" class="btn btn-primary"/>
    </form>
  </div>
</template>
```

```javascript
// Submit form event, in client/templates/posts/post_submit.js
// Calls serverside method.
Template.postSubmit.events({
  'submit form': function(e) {
    e.preventDefault();

    var post = {
      url: $(e.target).find('[name=url]').val(),
      title: $(e.target).find('[name=title]').val()
    };

    Meteor.call('postInsert', post, function(error, result) {
      // display the error to the user and abort
      if (error)
        return alert(error.reason);
      Router.go('postPage', {_id: result._id});  
    });
  }
});
```

```javascript
// Insert method, in lib/collections/posts.js
// Serverside method
// Brings parameters from form event, then adds user and date to post object
Meteor.methods({
  postInsert: function(postAttributes) {
    check(Meteor.userId(), String);
    check(postAttributes, {
      title: String,
      url: String
    });
    var user = Meteor.user();
    var post = _.extend(postAttributes, {
      userId: user._id, 
      author: user.username, 
      submitted: new Date()
    });
    var postId = Posts.insert(post);
    return {
      _id: postId
    };
  }
});
```

#### **Publication with nested collections**
This function allows subscribing to Posts and their first two comments.

```js
// At /server/publication.js
Meteor.publish('postsWithComments', function(options) {
  var sub = this, commentHandles = [], postHandle = null;

  // send over the top two comments attached to a single post
  function publishPostComments(postId) {
    var commentsCursor = Comments.find({postId: postId}, {sort: {submitted: -1, _id: -1}, limit: 2});
    commentHandles[postId] = 
      Mongo.Collection._publishCursor(commentsCursor, sub, 'comments');
  }

  postHandle = Posts.find({}, options).observeChanges({
    added: function(id, post) {
      publishPostComments(id);
      sub.added('posts', id, post);
    },
    changed: function(id, fields) {
      sub.changed('posts', id, fields);
    },
    removed: function(id) {
      // stop observing changes on the post's comments
      commentHandles[id] && commentHandles[id].stop();
      // delete the post
      sub.removed('posts', id);
    }
  });

  sub.ready();

  // make sure we clean everything up (note `_publishCursor`
  //   does this for us with the comment observers)
  sub.onStop(function() { postHandle.stop(); });
});
```

