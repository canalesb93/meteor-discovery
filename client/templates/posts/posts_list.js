Template.postsList.helpers({
  // The individual posts helper is at ROUTER.JS
  comments: function() {
    return Comments.find({postId: this._id});
  },
  loc: function () {
    // return 0, 0 if the location isn't ready
    return Geolocation.latLng() || { lat: 0, lng: 0 };
  },
  error: Geolocation.error
});

