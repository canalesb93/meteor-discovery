Template.errors.helpers({
  errors: function() {
    return Errors.find();
  }
});

Template.error.rendered = function() {
  var error = this.data;
  console.log(this);
  Meteor.setTimeout(function () {
    Errors.remove(error._id);
  }, 4000);
};
