Template.commentSubmit.created = function() {
  Session.set('commentSubmitErrors', {});
}

Template.commentSubmit.helpers({
  errorMessage: function(field) {
    return Session.get('commentSubmitErrors')[field];
  },
  errorClass: function (field) {
    return !!Session.get('commentSubmitErrors')[field] ? 'has-error' : '';
  }
});

Template.commentSubmit.events({
  'submit form, keypress form': function(e, template) {
    if ((e.type === 'submit') || (e.type === 'keypress' && e.which === 13) ) {
      e.preventDefault();

      var $body = $('#comment-body');
      // var $body = $(e.target).find('[name=body]');
      var comment = {
        body: $body.val(),
        postId: template.data._id
      };

      var errors = {};
      if (! comment.body) {
        errors.body = "Please write some content";
        return Session.set('commentSubmitErrors', errors);
      }

      Meteor.call('commentInsert', comment, function(error, commentId) {
        if (error){
          throwError(error.reason);
        } else {
          $body.val('');
        }
      });

      $('.comment').last().addClass('fadeWhite');
    }
  }
});